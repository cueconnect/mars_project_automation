package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.HomePageObject;
import pageobjects.ItemsPageObject;
import setup.SeleniumBaseTest;

/**
 * Created by vitaliybizilia on 3/16/17.
 */
public class ItemsPageTests extends SeleniumBaseTest {

    @Test
    public void itemsPageLoading(){

        HomePageObject homePageObject = new HomePageObject();
        ItemsPageObject itemsPageObject = homePageObject.goToItemsTab();
        Assert.assertEquals("Items Summery", itemsPageObject.getHeader());

    }
}
