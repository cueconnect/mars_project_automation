package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.HomePageObject;
import setup.SeleniumBaseTest;

/**
 * Created by vitaliybizilia on 3/16/17.
 */
public class HomePageTests extends SeleniumBaseTest{

    @Test
    public void pageTitleChecking(){
        HomePageObject homePageObject = new HomePageObject();
        Assert.assertEquals("This is Headline", homePageObject.getHeader());
    }
}
