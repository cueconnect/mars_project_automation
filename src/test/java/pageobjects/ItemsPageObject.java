package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static setup.SeleniumDriver.getDriver;

/**
 * Created by vitaliybizilia on 3/16/17.
 */
public class ItemsPageObject extends BasePageObject {
    public ItemsPageObject() {
        super(getDriver());

    }

    public String getHeader() {
        WebElement header = getDriver().findElement(By.xpath("//h1[@class='header-typo']"));
        fluentWaitforElement(header, 10, 5);
        return header.getText();
    }

}
