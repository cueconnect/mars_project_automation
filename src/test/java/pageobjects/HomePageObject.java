package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static setup.SeleniumDriver.getDriver;

/**
 * Created by vitaliybizilia on 3/16/17.
 */
public class HomePageObject extends BasePageObject {
    public HomePageObject() {
        super(getDriver());
        getDriver().get("https://qa-mars.cueconnect.net");
    }

    public ItemsPageObject goToItemsTab() {
        switchToTab("Items");
        return new ItemsPageObject();
    }

    public String getHeader() {
        waitFor(6);
        WebElement header = getDriver().findElement(By.xpath("//div[@class='header-typo']"));
        fluentWaitforElement(header, 10, 5);
        return header.getText();
    }


}
